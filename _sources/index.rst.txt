.. include:: /_static/isonum.txt

.. The Ubuntourist Trap master file, created by
   sphinx-quickstart on Sat Sep  5 17:13:07 2020.
   You can adapt this file completely to your liking, but it
   should at least contain the root `toctree` directive.

Welcome to The Ubuntourist Trap
===============================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   tips/index
   pdp-11/index


* `The whole enchilada as a PDF <pdf/UbuntouristTrap.pdf>`__

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
