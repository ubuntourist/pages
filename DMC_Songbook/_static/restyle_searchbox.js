// restyle_searchbox.js
// Extracted by Kevin Cole <ubuntourist@hacdc.org> 2022.11.22 (kjc)
//
// Extracted from the searchbox.html Jinja block snippet to satisfy
// Content Security Policy (CSP) anal retentiveness.
//

document.getElementById('searchbox').style.display = "block";
