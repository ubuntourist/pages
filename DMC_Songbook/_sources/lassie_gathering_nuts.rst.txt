.. Dupont Circle Songbook

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########################
The Lassie Gathering Nuts
#########################

.. score:: lassie_gathering_nuts

.. note::

   In the 1959 edition of "The Merry Muses of Caledonia", this song is
   listed under songs "Collected by Burns". Gershon Legman, in "The
   Horn Book", argues convincingly that it is a rewriting of a French
   students' and children's traditional song "Fillarette".
