.. Dupont Circle Songbook

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#############
Died for Love
#############

.. score:: died_for_love

.. note::

   One of the classic songs of desertion. There are a multitude of
   variations ("A brisk young sailor courted me...") and fragments
   have broken off and other songs have been formed from them. The
   Cornish version, "There is a Tavern in the Town," is well
   known in America.
