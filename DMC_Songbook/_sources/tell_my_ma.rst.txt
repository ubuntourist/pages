.. Dupont Circle Songbook

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###############
I'll Tell My Ma
###############

.. score:: tell_my_ma

.. note::

   Every town in Ireland has its own version of this children's sweet
   song.
