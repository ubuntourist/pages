.. Dupont Circle Songbook

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########
Foggy Dew
#########

.. score:: foggy_dew

.. note::

   This song of the Easter rising of 1916 was an appeal to Irishmen to
   die fighting for their own country rather than die in some foreign
   war in a British uniform. Sulva was the battleground in the Middle
   East. "Pearse" and "Valera", mentioned in the third stanza, refer
   to Patrick Pearse leader of the Easter rising, and Eamon de Valera,
   who later became prime minister of Ireland.
