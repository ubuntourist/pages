.. Dupont Circle Songbook

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

####################
The Band o' Shearers
####################

.. score:: band_o_shearers

.. note::

   In Ord's introduction to **'Bothy Songs and Ballads'** there is an
   interesting note concerning shearers: *"The shearing was mostly done
   by women. The value of the day's work was calculated by the number
   of thraives cut. A thraive consisted of two stooks of twelve
   sheaves each.  To cut seven or eight sheaves was considered a good
   day's work for a shearer.  After the introduction of the scythe
   (1810), the best men cut the corn, the women gathered into sheaves
   and made the bands, while younger men, as a rule, bound and stooked
   the sheaves. The bandster could claim a kiss from the gatherer for
   each band whose knot slipped in the binding."*
