.. Dupont Circle Songbook

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##################
Four Partner Songs
##################

.. score:: four_partner_songs

.. note::

   Not originally part of the Dupont Music Circle Songbook, but it
   "fit the bill" and is thus a late addition.

.. note::

   I transposed each of the four songs to a different octave to create
   a sense of SATB.
