.. Page 107 (PDF Page 57, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#################
Jock o' Hazeldean
#################

.. score:: scotch/jock_hazeldean

----

(Source: `The Celtic Song Book, p. 107
<https://archive.org/details/the-celtic-song-book/page/n56/mode/1up>`_)
