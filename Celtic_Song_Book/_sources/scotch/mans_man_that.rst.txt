.. Page 99 (PDF Page 53, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########################
A Man's a Man for a' That
#########################

.. score:: scotch/mans_man_that

----

(Source: `The Celtic Song Book, p. 99
<https://archive.org/details/the-celtic-song-book/page/n52/mode/1up>`_)
