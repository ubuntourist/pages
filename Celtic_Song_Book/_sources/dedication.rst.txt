.. Page 0 (PDF Page 4, Left)

.. include:: <isonum.txt>
.. include:: _static/isonum.txt

.. rst-class:: center

##########
DEDICATION
##########

.. container:: center

   | TO
   | A. E. S. G

.. container:: left

   | My Amy, *bien aimée*,
   |   How poor had been my part
   | At garnering these folk songs
   |   Without your tuneful art;
   | When your voice and hands commingled
   |   O'er the black notes and the white,
   | As song by song we singled
   |   With ever fresh delight;

   | Until their royal roses,
   |   Their wild flowers blush and pale,
   | We've set into six posies
   |   Of the Cymro and the Gael.
   | And since to you thrice over
   |   Their woven wealth belongs,
   | From your husband and your lover
   |   Take this wreath of Celtic songs.

.. container:: center

   | ALFRED PERCEVAL GRAVES

.. container:: left

   *June* 12, 1926.

----

(Source: `The Celtic Song Book, Dedication
<https://archive.org/details/the-celtic-song-book/page/n3/mode/1up>`_)
