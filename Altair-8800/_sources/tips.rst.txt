.. include:: <isonum.txt>
.. include:: <isogrk3.txt>
.. include:: /_static/isonum.txt
.. |_|   unicode:: U+000A0 .. NO-BREAK SPACE
.. role:: raw-html(raw)
   :format: html

####
TIPS
####

Starting with **Chapter 6: Utility Programs** in the **Macroassembler
AS** manual (page 251).

Working from the object file produced by ``add.asm``...

**plist** summarizes ``.p`` file data::

    $ plist add.p
    PLIST/C V1.42 Beta [Bld 174]
    (C) 1992,2020 Alfred Arnold

    Code-Type   Segment    Start-Address  Length (Bytes) End-Address
    ----------------------------------------------------------------
    8080/8085     CODE      00000000          000E       0000000D
    8080/8085     CODE      00000080          0003       00000082
    creator : AS 1.42 Beta [Bld 174]/x86_64-unknown-linux

    altogether 17 bytes CODE

**plist** was able to analyze the object file and determine:

* the object code contained machine op codes for an Intel 8080 or
  Intel 8085.
* a code block of 14 bytes supposed to be loaded into memory locations
  000 to 00D (hex), a.k.a. 000 to 015 (octal)
* a code block of 3 bytes should be loaded into 080 to 082 (hex), a.k.a.
  200 to 202 (octal)

----

**p2hex** converts a ``.p`` object file to an ASCII representation of the
file (``.hex``). There are several different format available for the
output file, based in part on the target machine.  In our example::

    $ p2hex add.p
    $ most add.hex
    :0E0000003A8000473A810080328200C300003F
    :0300800005080070
    :00000001FF

Looking at the above, and comparing it to the hexadecimal list file
produced by the assembler, we can determine that the first two lines
represent the two data segments:

.. container:: center

   .. table:: HEX record layout
      :header-alignment: center center center
      :column-alignment: center center left
      :column-dividers: single single double single

      +-------------+-----------------------------------------+
      |   Columns   |                Interpretation           |
      +-------+-----+-----------------------------------------+
      | Start | End |                                         |
      +=======+=====+=========================================+
      |   1   |  1  | Colon. Start of record delimiter        |
      +-------+-----+-----------------------------------------+
      |   2   |  3  | Lenghth of data in record               |
      +-------+-----+-----------------------------------------+
      |   4   |  7  | Starting address in memory for data     |
      +-------+-----+-----------------------------------------+
      |   8   |  9  | ???? possibly a record type? (00 or 01) |
      +-------+-----+-----------------------------------------+
      |  10   | N-2 | Data                                    |
      +-------+-----+-----------------------------------------+
      |  N-1  |  N  | ???? possibly a record checksum?        |
      +-------+-----+-----------------------------------------+

The first two lines contain 00 in columns 8-9, while the last, which does
not appear to have any relation to the machine code shown in the list file,
contains 01. Perhaps an "End of Data" record?

Lo and behold, Wikipedia (naturally) has an entry on `Intel HEX`_
which confirms, yea verily,

* 8-9 is a record type indicator, with ``00`` being **data** and ``01``
  being **end of file**
* the last two characters are a checksum

The entry even offers an example of the `checksum calculation
algorithm`_.

----

**p2bin** converts a ``.p`` file to a binary memory dump, without formatting.
By default, uninitialized bytes become ``FF``. Although it could potentially
take a very long time to load, this, on the surface, appears to be the most
straight-forward file.

----

.. _Intel HEX: https://en.wikipedia.org/wiki/Intel_HEX
.. _checksum calculation algorithm: https://en.wikipedia.org/wiki/Intel_HEX#Checksum_calculation

